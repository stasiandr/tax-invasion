using UnityEngine;
using UnityEngine.Events;

namespace Environment
{
    public class QuestGiver : MonoBehaviour
    {
        public UnityEvent questCompleted;
        
        public bool IsQuestAccepted { get; set; }
        public bool IsQuestCompleted { get; private set; }

        public void CompleteQuest()
        {
            questCompleted.Invoke();
            IsQuestCompleted = true;
        }

    }
}