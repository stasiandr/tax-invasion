using Mics;
using NaughtyAttributes;
using Nrjwolf.Tools.AttachAttributes;
using UnityEngine;

namespace Environment
{
    public class Campfire : MonoBehaviour
    {
        [SerializeField] [GetComponent]
        private Interactable interactable;

        public GameObject fire;
        
        [ReadOnly]
        public int woodPlaced;

        public void PlaceWood()
        {
            interactable.DestroyHoldingObject();

            if (++woodPlaced != 0)
            {
                fire.SetActive(true);
            }
        }

        public bool HowManyWoodPlaced(int wood) => woodPlaced == wood;
    }
}