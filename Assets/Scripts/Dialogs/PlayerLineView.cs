using DialogueGraph.Runtime;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Dialogs
{
    public class PlayerLineView : MonoBehaviour
    {
        [SerializeField]
        private Button btn;
        
        [SerializeField]
        private TMP_Text text;

        public void Construct(ConversationLine line, UnityAction callback, bool active)
        {
            btn.onClick.AddListener(callback);
            text.text = line.Message;
            
            if (active) btn.Select();
        }
    }
}