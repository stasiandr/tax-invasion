using System;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using DialogueGraph.Runtime;
using UnityEngine;

namespace Dialogs
{
    public class PlayerSelectLineViewController : MonoBehaviour
    {
        public PlayerLineView playerLine;
        
        private int? _selectedAnswer;
        private void Awake()
        {
            playerLine.gameObject.SetActive(false);
        }

        public async UniTask<int> SelectAnswer(ActorData actorData, List<ConversationLine> lines)
        {
            _selectedAnswer = null;

            foreach (Transform child in playerLine.transform.parent)
            {
                if (child != playerLine.transform)
                    Destroy(child.gameObject);
            }
            
            gameObject.SetActive(true);

            for (var i = 0; i < lines.Count; i++)
            {
                var line = lines[i];
                var go = Instantiate(playerLine, playerLine.transform.parent);

                var selection = i;
                go.Construct(line, () => _selectedAnswer = selection, i == 0);

                go.gameObject.SetActive(true);
            }

            await UniTask.WaitUntil(() => _selectedAnswer.HasValue);
            
            gameObject.SetActive(false);
            
            return _selectedAnswer!.Value;
        }
    }
}