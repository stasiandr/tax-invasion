using System;
using System.Linq;
using Cysharp.Threading.Tasks;
using DialogueGraph.Runtime;
using Nrjwolf.Tools.AttachAttributes;
using UnityEngine;
using UnityEngine.Serialization;

namespace Dialogs
{
    public class DialogController : MonoBehaviour
    {
        public static DialogController Instance;

        [SerializeField] [GetComponent] private Canvas canvas;

        public NPCLinesView npcLinesView;
        public PlayerSelectLineViewController playerSelectLine;

        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.SubsystemRegistration)]
        public static void ResetStatic() => Instance = null;

        private void Awake()
        {
            Instance = this;
        }

        public static async UniTask StartDialog(RuntimeDialogueGraph dialog) => await Instance.StartDialogAsync(dialog);

        private async UniTask StartDialogAsync(RuntimeDialogueGraph dialog)
        {
            canvas.enabled = true;
            
            dialog.ResetConversation();

            while (!dialog.IsConversationDone())
            {
                if (dialog.IsCurrentNpc())
                {
                    var actor = dialog.GetCurrentActor();

                    var line = dialog.ProgressNpc();

                    await npcLinesView.Say(actor, line);
                }
                else
                {
                    var actor = dialog.GetCurrentActor();
                    var lines = dialog.GetCurrentLines();
                    
                    dialog.ProgressSelf(await playerSelectLine.SelectAnswer(actor, lines));
                }

                await UniTask.Yield();
            }
            
            canvas.enabled = false;
        }
    }
}