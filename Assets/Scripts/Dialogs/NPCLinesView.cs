using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using DialogueGraph.Runtime;
using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Dialogs
{
    public class NPCLinesView : MonoBehaviour
    {
        [SerializeField] 
        private TMP_Text npcNameDisplay;
        
        [SerializeField] 
        private TMP_Text messageDisplay;

        public async UniTask Say(ActorData actor, string line)
        {
            if (string.IsNullOrEmpty(line)) return;
            
            gameObject.SetActive(true);
            
            npcNameDisplay.text = actor != null ? actor.Name : "";
            
            messageDisplay.text = line;

            await UniTask.WaitUntil(() => 
                Input.GetKeyDown(KeyCode.Space) || 
                Input.GetKeyDown(KeyCode.Return) || 
                Input.GetMouseButtonDown(0));

            gameObject.SetActive(false);
        }
    }
}