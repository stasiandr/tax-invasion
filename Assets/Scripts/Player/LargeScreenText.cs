using System;
using Cysharp.Threading.Tasks;
using DG.Tweening;
using TMPro;
using UnityEngine;

namespace Player
{
    public class LargeScreenText : MonoBehaviour
    {
        public static LargeScreenText Instance;
        
        [SerializeField]
        private TMP_Text display;

        private void Start()
        {
            display.DOFade(0, 0);
            Instance = this;
        }

        public static void ShowTextStatic(string text, float duration = 2f)
        {
            Instance.ShowText(text, duration);
        }

        public void ShowText(string text, float duration = 2)
        {
            UniTask.Create(async () =>
            {
                display.text = text;
                display.transform.DOScale(1.4f, duration);
                await display.DOFade(1, .1f).SetEase(Ease.InCubic);
                await display.DOFade(0, duration - .1f).SetEase(Ease.InCubic);
            });
        }
    }
}