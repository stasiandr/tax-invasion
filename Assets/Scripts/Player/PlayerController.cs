using System;
using Cysharp.Threading.Tasks;
using DG.Tweening;
using Mics;
using Nrjwolf.Tools.AttachAttributes;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;
using UnityEngine.Serialization;

namespace Player
{
    public class PlayerController : MonoBehaviour
    {
        public static Transform PlayerHead;

        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.SubsystemRegistration)]
        public static void ResetStatics()
        {
            PlayerHead = null;
        }
        
        [SerializeField] [GetComponentInChildren]
        private Animator animator;
        
        [SerializeField] [GetComponent]
        private NavMeshAgent agent;

        [SerializeField]
        private AudioSource footsteps;

        [SerializeField] 
        private Transform pickPoint;

        [SerializeField]
        private Transform playerHead;

        [SerializeField]
        private PickableCanvasView pickableView;

        public GameObject hat;

        private static readonly int Speed = Animator.StringToHash("Speed");

        private static LayerMask Ground => LayerMask.GetMask("Ground");
        
        private Interactable _movingTowards;

        private void Awake()
        {
            PlayerHead = playerHead;
        }

        private void Update()
        {   
            animator.SetFloat(Speed, agent.velocity.magnitude);

            footsteps.enabled = agent.velocity.magnitude > 0.01f;
            
            // if (agent.velocity.magnitude > 0.01f && !footsteps.isPlaying)
            //     footsteps.Play();
            // else
            //     footsteps.Stop();

            if (_movingTowards != null && agent.remainingDistance < agent.stoppingDistance)
            {
                var interactionTarget = _movingTowards;
                agent.ResetPath();
                agent.velocity = Vector3.zero;
                _movingTowards = null;
                animator.SetFloat(Speed, 0);

                enabled = false;
                footsteps.enabled = false;

                UniTask.Create(async () =>
                {
                    await interactionTarget.StartInteraction(this);
                    enabled = true;
                });
                
                return;
            } 

            if (!Mouse.current.leftButton.isPressed) return;
            
            if (EventSystem.current.IsPointerOverGameObject()) return;
            
            var ray = Camera.main!.ScreenPointToRay(Mouse.current.position.ReadValue());

            if (Physics.Raycast(ray, out var interactableHit, 1000, PlayerOutlineController.Interactables))
            {
                _movingTowards = interactableHit.transform.GetComponentInParent<Interactable>();

                if (_movingTowards == null || !_movingTowards.enabled) return;
                
                var target = _movingTowards.transform.position;
                agent.SetDestination(target + (transform.position - target).normalized * agent.stoppingDistance);

                return;
            }

            _movingTowards = null;

            if (!Physics.Raycast(ray, out var hit, 1000, Ground)) return;

            agent.SetDestination(hit.point);
        }

        public Pickable PickedObject { get; private set; }

        public void Pick(Pickable pickable)
        {
            pickable.transform.SetParent(pickPoint);
            pickable.transform.DOLocalMove(Vector3.zero, .3f);
            pickable.transform.DOLocalRotate(Vector3.zero, .3f);
            pickableView.ViewPickable(pickable, Throw);

            PickedObject = pickable;
        }

        public void Throw()
        {
            PickedObject.transform.SetParent(null);
            PickedObject.transform.DOMove(transform.position, .3f);

            PickedObject = null;
        }

        public void DestroyHoldingObject()
        {
            if (PickedObject == null) return;
            
            pickableView.HidePickable();
            
            Destroy(PickedObject.gameObject);
            PickedObject = null;
        }
    }
}
