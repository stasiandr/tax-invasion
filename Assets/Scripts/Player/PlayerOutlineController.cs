using Mics;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Player
{
    public class PlayerOutlineController : MonoBehaviour
    {
        public static LayerMask Interactables => LayerMask.GetMask("Interactable", "InteractableOutlined");
        
        private Outlineable _outlineable;
        private void Update()
        {
            var ray = Camera.main!.ScreenPointToRay(Mouse.current.position.ReadValue());

            Outlineable outlineable = null;
            if (Physics.Raycast(ray, out var hit, 1000, Interactables))
                outlineable = hit.transform.GetComponentInParent<Outlineable>();

            if (_outlineable != null) _outlineable.SetOutlined(false);
            _outlineable = outlineable;
            if (_outlineable != null) _outlineable.SetOutlined(true);
        }
        
    }
}