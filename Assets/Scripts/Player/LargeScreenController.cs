using UnityEngine;

namespace Player
{
    [CreateAssetMenu]
    public class LargeScreenController : ScriptableObject
    {
        public void ShowText(string text, float duration = 2f)
        {
            LargeScreenText.ShowTextStatic(text, duration);
        }
    }
}