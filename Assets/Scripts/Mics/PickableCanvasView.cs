using System;
using Cysharp.Threading.Tasks;
using DG.Tweening;
using Nrjwolf.Tools.AttachAttributes;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Mics
{
    public class PickableCanvasView : MonoBehaviour
    {
        public Image targetImage;
        public CanvasGroup group;

        public TMP_Text helpText;


        public static bool HelpTextShown;
        private Action _callback;

        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.SubsystemRegistration)]
        public static void ResetStatics()
        {
            HelpTextShown = false;
        }
        
        private void Start()
        {
            group.alpha = 0;
            group.blocksRaycasts = false;
        }

        public void ViewPickable(Pickable pickable, Action callback)
        {
            _callback = callback;
            
            targetImage.sprite = pickable.image;
            group.DOFade(1, .5f);
            group.blocksRaycasts = true;

            if (HelpTextShown) return;
            
            HelpTextShown = true;

            UniTask.Create(async () =>
            {
                await helpText.DOFade(1, .3f);

                await UniTask.Delay(1000 * 5);
                await helpText.DOFade(0, .3f);
            });
        }

        public void Throw()
        {
            _callback.Invoke();
            HidePickable();
        }

        public void HidePickable()
        {
            group.blocksRaycasts = false;
            group.DOFade(0, .5f).onComplete += () =>
            {
                targetImage.sprite = null;
            };
        }

    }
}