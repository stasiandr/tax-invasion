using System;
using Cinemachine;
using Cysharp.Threading.Tasks;
using Dialogs;
using DialogueGraph.Runtime;
using Nrjwolf.Tools.AttachAttributes;
using Player;
using UnityEngine;

namespace Mics
{
    [SelectionBase]
    public class Interactable : MonoBehaviour
    {
        [GetComponentInChildren] 
        [SerializeField]
        private new CinemachineVirtualCamera camera;
        
        [GetComponentInChildren] 
        [SerializeField]
        private RuntimeDialogueGraph dialog;

        protected PlayerController PlayerController;

        private void OnEnable()
        {
            if (camera != null) camera.enabled = false;
        }

        public async UniTask StartInteraction(PlayerController playerController)
        {
            PlayerController = playerController;
            if (camera != null) camera.enabled = true;
            
            await DialogController.StartDialog(dialog);

//            AfterInteraction(playerController);

            if (camera != null) camera.enabled = false;
        }

        public bool IsHoldingWood(string node, int lineIndex)
        {
            return true; //IsHolding("wood");
        }
        
        public void PickItem()
        {
            if (this is not Pickable) return;
            
            PlayerController.Pick((Pickable) this);
            enabled = false;
        }

        public void SetPlayerHat(bool isActive) => PlayerController.hat.SetActive(isActive);

        public void DestroyHoldingObject()
        {
            PlayerController.DestroyHoldingObject();
        }

        public bool IsHolding(string itemName) => PlayerController.PickedObject != null && itemName == PlayerController.PickedObject.ItemName;
    }
}