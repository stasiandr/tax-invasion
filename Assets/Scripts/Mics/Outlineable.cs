using Player;
using UnityEngine;

namespace Mics
{
    public class Outlineable : MonoBehaviour
    {
        [SerializeField]
        private GameObject[] outline;

        public bool IsOutlined => outline.Length != 0 && outline[0].layer == LayerMask.NameToLayer("InteractableOutlined");
        
        public void SetOutlined(bool outlined)
        {
            foreach (var meshRender in GetComponentsInChildren<Renderer>())
            {
                meshRender.gameObject.layer = LayerMask.NameToLayer(outlined ? "InteractableOutlined" : "Interactable");
            }
        }
    }
}