using System;
using Cysharp.Threading.Tasks;
using Player;
using UnityEngine;

namespace Mics
{
    public class FinishTrigger : MonoBehaviour
    {
        [SerializeField]
        private LargeScreenController text;

        [SerializeField] private LevelManager manager;

        public GameObject finishCondition;
        
        private void OnTriggerEnter(Collider other)
        {
            if (finishCondition.activeSelf) return;
            
            text.ShowText("My love, is that you?");

            UniTask.Create(async () =>
            {
                await UniTask.Delay(1000 * 2);
                manager.LoadFinish();
            });
        }
    }
}