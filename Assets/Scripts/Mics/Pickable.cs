using Player;
using UnityEngine;

namespace Mics
{
    public class Pickable : Interactable
    {
        public Sprite image;
        
        [field: SerializeField]
        public string ItemName { get; set; }
    }
}