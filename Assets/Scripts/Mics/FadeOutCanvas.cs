using DG.Tweening;
using Nrjwolf.Tools.AttachAttributes;
using UnityEngine;

namespace Mics
{
    public class FadeOutCanvas : MonoBehaviour
    {
        [SerializeField] [GetComponent]
        private CanvasGroup group;

        public float duration = .3f;
        
        public void Fade(float targetAlpha)
        {
            group.DOFade(targetAlpha, duration);
        }
    }
}