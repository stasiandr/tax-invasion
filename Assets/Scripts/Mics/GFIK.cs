using System;
using Nrjwolf.Tools.AttachAttributes;
using Player;
using UnityEngine;

namespace Mics
{
    public class GFIK : MonoBehaviour
    {
        [GetComponent] [SerializeField]
        private Animator animator;
        private void OnAnimatorIK(int layerIndex)
        {
            if (PlayerController.PlayerHead == null)
            {
                animator.SetLookAtWeight(0);
                return;
            }

            if (Vector3.Distance(PlayerController.PlayerHead.position, transform.position) > 3)
            {
                animator.SetLookAtWeight(0);
                return;
            }
            
            animator.SetLookAtWeight(1);
            animator.SetLookAtPosition(PlayerController.PlayerHead.position);
        }
    }
}