using Cysharp.Threading.Tasks;
using DG.Tweening;
using NaughtyAttributes;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Mics
{
    [CreateAssetMenu]
    public class LevelManager : ScriptableObject
    {
        [Scene]
        public string firstWorld;

        [Scene]
        public string secondWorld;
        
        [Scene]
        public string thirdWorld;
        
        [Scene]
        public string menu;
        
        [Scene]
        public string player;
        
        [Scene]
        public string finishWorld;

        public Material shutter;

        public void LoadFirstWorld() => LoadLevel(firstWorld);
        public void LoadSecondWorld() => LoadLevel(secondWorld);
        
        public void LoadThirdWorld() => LoadLevel(thirdWorld);

        public void LoadFinish() => LoadLevel(finishWorld);
        
        public void LoadMenu() => LoadLevel(menu);

        
        private void LoadLevel(string levelName)
        {
            UniTask.Create(async () =>
            {
                var handle = SceneManager.LoadSceneAsync(levelName, LoadSceneMode.Additive);
                handle.allowSceneActivation = false;

                await shutter.DOFloat(-0.1f, "_Alpha", 1f).SetEase(Ease.OutCubic).ToUniTask();

                handle.allowSceneActivation = true;
                await handle;

                if (levelName != menu)
                {
                    if (!SceneManager.GetSceneByName(player).isLoaded)
                        await SceneManager.LoadSceneAsync(player, LoadSceneMode.Additive);
                }

                await SceneManager.UnloadSceneAsync(SceneManager.GetActiveScene());
                SceneManager.SetActiveScene(SceneManager.GetSceneByName(levelName));

                await shutter.DOFloat(1.1f, "_Alpha", 1f).SetEase(Ease.InCubic);

                Debug.Log($"{firstWorld} loaded");
            });
        }


        
        
    }
}