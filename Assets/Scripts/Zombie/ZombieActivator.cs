using System;
using Player;
using UnityEngine;

namespace Zombie
{
    public class ZombieActivator : MonoBehaviour
    {
        public GameObject[] zombies;
        
        private void OnTriggerEnter(Collider other)
        {
            if (!other.attachedRigidbody.TryGetComponent<PlayerController>(out _)) return;

            foreach (var go in zombies)
            {
                go.SetActive(true);
            }
        }
    }
}