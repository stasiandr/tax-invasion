using System;
using System.Collections;
using Mics;
using Nrjwolf.Tools.AttachAttributes;
using Player;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;

namespace Zombie
{
    public class ZombieController : MonoBehaviour
    {
        [SerializeField] [GetComponent]
        private NavMeshAgent agent;

        public UnityEvent playerWon;
        public UnityEvent playerDead;

        public IEnumerator Start()
        {
            var speed = agent.speed;
            agent.speed = 0;
            yield return new WaitForSeconds(5);
            agent.speed = speed;
        }

        private void Update()
        {
            if (PlayerController.PlayerHead == null) return;
            
            agent.SetDestination(PlayerController.PlayerHead.position);

            if (Vector3.Distance(PlayerController.PlayerHead.position, transform.position) < 0.32f)
            {
                var player = PlayerController.PlayerHead.GetComponentInParent<PlayerController>();
                var itemName = player.GetComponentInChildren<Pickable>().ItemName;
                
                if (itemName is "shovel" or "wood")
                {
                    player.DestroyHoldingObject();
                    playerWon.Invoke();
                }
                else playerDead.Invoke();

                enabled = false;
                gameObject.SetActive(false);
            }
        }
    }
}