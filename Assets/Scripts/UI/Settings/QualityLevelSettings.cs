using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.UI;

namespace UI.Settings
{
    public class QualityLevelSettings : MonoBehaviour
    {
        [SerializeField]
        private Button low;
        
        [SerializeField]
        private Button high;

        private Dictionary<QualityLevel, Button> Buttons => new()
        {
            { QualityLevel.Low, low },
            { QualityLevel.High, high }
        };


        private enum QualityLevel
        {
            Low = 0, 
            High = 1,
        }
        
        
        private void Start()
        {
            SetSetting(LoadSetting());
            
            low.onClick.AddListener(() => SetSetting(QualityLevel.Low));
            high.onClick.AddListener(() => SetSetting(QualityLevel.High));
        }


        private void SetSetting(QualityLevel qualityLevel)
        {
            PlayerPrefs.SetString("QualityLevel", qualityLevel.ToString());

            foreach (var btn in Buttons.Values) SetFontStyle(btn, FontStyles.Normal);
            SetFontStyle(Buttons[qualityLevel], FontStyles.Bold);
            
            
            QualitySettings.SetQualityLevel((int) qualityLevel);
        }

        private void SetFontStyle(Button btn, FontStyles style)
        {
            btn.GetComponentInChildren<TMP_Text>().fontStyle = style;
        }

        private QualityLevel LoadSetting()
        {
            if (Enum.TryParse<QualityLevel>(PlayerPrefs.GetString("QualityLevel"), true, out var value))
            {
                return value;
            }

            SetSetting(QualityLevel.High);
            return LoadSetting();
        }
    }
}