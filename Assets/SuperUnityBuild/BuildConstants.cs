using System;

// This file is auto-generated. Do not modify or move this file.

public static class BuildConstants
{
    public enum ReleaseType
    {
        None,
        dev,
        prod,
    }

    public enum Platform
    {
        None,
        WebGL,
    }

    public enum Architecture
    {
        None,
        WebGL,
    }

    public enum Distribution
    {
        None,
    }

    public static readonly DateTime buildDate = new DateTime(638255636833937020);
    public const string version = "1.0.0.1";
    public const ReleaseType releaseType = ReleaseType.dev;
    public const Platform platform = Platform.WebGL;
    public const Architecture architecture = Architecture.WebGL;
    public const Distribution distribution = Distribution.None;
}

